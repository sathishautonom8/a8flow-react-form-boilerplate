# a8flow React-form Boilerplate 

## Instructions

###  Clone this repo and install dependencies using below cmd : 
 -  Run `npm install`

### For Development : 
- Run `npm start`, **localhost:8080** will open up in your default browser

### For Production : 
- Run `npm build`, it will create /dist folder with  compiled code.
 ```
  // dist folder contain following structure
  
  ./dist/
        /index.html
        /main.js //use this file where you want to render the form
 ```

### Flow Configuration : 
 - Run `npm flow`, for checking flow logs.
 - use flow extensions to detect the errors at runtime.
 - Flow extensions for vscode : 
   - https://marketplace.visualstudio.com/items?itemName=gcazaciuc.vscode-flow-ide

   - https://marketplace.visualstudio.com/items?itemName=flowtype.flow-for-vscode
 