//@flow
import * as React from 'react'

type Props = {
    foo: number,
    bar?: string,
  };

type State = {
 count : number
}

class Form extends React.Component<Props,State> {
    
    state = {
        count : 2
    }

    handleClick = ()=>{
        console.log("this is working");
    }

    render() {
        return (
            <React.Fragment>
                <h1>Form Title</h1>
                <input type="text" value={"sathish kumar"} onChange={()=>console.log("fromhere")} ></input>
                <input type="submit" value="submit" onClick={this.handleClick}></input>
            </React.Fragment>
        );
    }
}

export default Form;